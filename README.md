# README #

1/3スケールのマウスコンピューター デスクトップ・ミニタワーPC Gtune風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- マウスコンピューター

## 発売時期

- -

## 参考資料

- マウスコンピューター デスクトップ・ミニタワーPC(https://www.mouse-jp.co.jp/store/r/ra3020101/)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_gtune/raw/91fdbd72733355b7af21cc1bc7b6ec1656e15cc1/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_gtune/raw/91fdbd72733355b7af21cc1bc7b6ec1656e15cc1/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_gtune/raw/91fdbd72733355b7af21cc1bc7b6ec1656e15cc1/ExampleImage.png)
